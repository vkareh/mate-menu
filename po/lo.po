# Lao translation for MATE Menu
# Copyright (c) 2011 Rosetta Contributors and Canonical Ltd 2011
# This file is distributed under the same license as the package.
# FIRST AUTHOR <EMAIL@ADDRESS>, 2011.
#
msgid ""
msgstr ""
"Project-Id-Version: mate-menu\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2017-05-14 00:28+0100\n"
"PO-Revision-Date: 2014-04-24 16:00+0000\n"
"Last-Translator: Rockworld <sumoisrock@gmail.com>\n"
"Language-Team: Lao <lo@li.org>\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Launchpad-Export-Date: 2015-02-25 10:21+0000\n"
"X-Generator: Launchpad (build 17355)\n"

#: ../lib/mate-menu.py:69
msgid "Menu"
msgstr "ເມນູ"

#. Fake class for MyPlugin
#: ../lib/mate-menu.py:236
msgid "Couldn't load plugin:"
msgstr "ບໍ່ສາມາດໂລດໂປຣເເກຣມເສີມ:"

#: ../lib/mate-menu.py:309
msgid "Couldn't initialize plugin"
msgstr ""

#: ../lib/mate-menu.py:740
msgid "Advanced MATE Menu"
msgstr ""

#: ../lib/mate-menu.py:834
msgid "Preferences"
msgstr "ການປັບແຕ່ງຄ່າ"

#: ../lib/mate-menu.py:837
msgid "Edit menu"
msgstr "ເມນູການປັບເເຕ່ງ"

#: ../lib/mate-menu.py:840
msgid "Reload plugins"
msgstr "ໂລດໂປຣເເມເສົມອີກເທື່ອ"

#: ../lib/mate-menu.py:843
msgid "About"
msgstr "ກ່ຽວກັບ"

#. i18n
#: ../lib/mate-menu-config.py:53
msgid "Menu preferences"
msgstr ""

#: ../lib/mate-menu-config.py:56
msgid "Always start with favorites pane"
msgstr ""

#: ../lib/mate-menu-config.py:57
msgid "Show button icon"
msgstr ""

#: ../lib/mate-menu-config.py:58
msgid "Use custom colors"
msgstr ""

#: ../lib/mate-menu-config.py:59
msgid "Show recent documents plugin"
msgstr ""

#: ../lib/mate-menu-config.py:60
msgid "Show applications plugin"
msgstr ""

#: ../lib/mate-menu-config.py:61
msgid "Show system plugin"
msgstr ""

#: ../lib/mate-menu-config.py:62
msgid "Show places plugin"
msgstr ""

#: ../lib/mate-menu-config.py:64
msgid "Show application comments"
msgstr ""

#: ../lib/mate-menu-config.py:65
msgid "Show category icons"
msgstr ""

#: ../lib/mate-menu-config.py:66
msgid "Hover"
msgstr ""

#: ../lib/mate-menu-config.py:67
msgid "Remember the last category or search"
msgstr ""

#: ../lib/mate-menu-config.py:68
msgid "Swap name and generic name"
msgstr "ປ່ຽນຊື່ ເເລະ ຊື່ປອມ"

#: ../lib/mate-menu-config.py:70
msgid "Border width:"
msgstr ""

#: ../lib/mate-menu-config.py:71
msgid "pixels"
msgstr "ພິກໂຊ"

#: ../lib/mate-menu-config.py:73
msgid "Button text:"
msgstr ""

#: ../lib/mate-menu-config.py:74
msgid "Options"
msgstr "ບັນດາຕົວເລືອກ"

#: ../lib/mate-menu-config.py:75 ../mate_menu/plugins/applications.py:189
msgid "Applications"
msgstr "ໂປຣເເກຣມທົວໄປ"

#: ../lib/mate-menu-config.py:77
msgid "Theme"
msgstr ""

#: ../lib/mate-menu-config.py:78 ../mate_menu/plugins/applications.py:186
#: ../mate_menu/plugins/applications.py:187
msgid "Favorites"
msgstr ""

#: ../lib/mate-menu-config.py:79
msgid "Main button"
msgstr "ປູ່ມຫລັກ"

#: ../lib/mate-menu-config.py:80
msgid "Plugins"
msgstr ""

#: ../lib/mate-menu-config.py:82
msgid "Background:"
msgstr "ພື້ນ:"

#: ../lib/mate-menu-config.py:83
msgid "Headings:"
msgstr ""

#: ../lib/mate-menu-config.py:84
msgid "Borders:"
msgstr "ຂອບ:"

#: ../lib/mate-menu-config.py:85
msgid "Theme:"
msgstr ""

#. self.builder.get_object("applicationsLabel").set_text(_("Applications"))
#. self.builder.get_object("favoritesLabel").set_text(_("Favorites"))
#: ../lib/mate-menu-config.py:89
msgid "Number of columns:"
msgstr ""

#: ../lib/mate-menu-config.py:90 ../lib/mate-menu-config.py:91
#: ../lib/mate-menu-config.py:92 ../lib/mate-menu-config.py:93
msgid "Icon size:"
msgstr "ຂະຫນາດ ໄອຄອນ:"

#: ../lib/mate-menu-config.py:94
msgid "Hover delay (ms):"
msgstr ""

#: ../lib/mate-menu-config.py:95
msgid "Button icon:"
msgstr "ປູ່ມ ໄອຄອນ:"

#: ../lib/mate-menu-config.py:96
msgid "Search command:"
msgstr "ຄໍາຊັ່ງ ຊອກຫາ:"

#. Set 'heading' property for plugin
#: ../lib/mate-menu-config.py:98 ../mate_menu/plugins/places.py:54
msgid "Places"
msgstr "ສະຖານທີ່"

#: ../lib/mate-menu-config.py:99 ../lib/mate-menu-config.py:111
msgid "Allow Scrollbar"
msgstr ""

#: ../lib/mate-menu-config.py:100
msgid "Show GTK+ Bookmarks"
msgstr ""

#: ../lib/mate-menu-config.py:101 ../lib/mate-menu-config.py:112
msgid "Height:"
msgstr "ຄວາມສູງ:"

#: ../lib/mate-menu-config.py:102
msgid "Toggle Default Places:"
msgstr ""

#: ../lib/mate-menu-config.py:103 ../mate_menu/plugins/places.py:149
msgid "Computer"
msgstr "ຄອມພິວເຕີ"

#: ../lib/mate-menu-config.py:104 ../mate_menu/plugins/places.py:160
msgid "Home Folder"
msgstr ""

#: ../lib/mate-menu-config.py:105 ../mate_menu/plugins/places.py:173
msgid "Network"
msgstr "ເຄືອຂ່າຍ"

#: ../lib/mate-menu-config.py:106 ../mate_menu/plugins/places.py:196
msgid "Desktop"
msgstr "ຕັ້ງໂຕ່"

#: ../lib/mate-menu-config.py:107 ../mate_menu/plugins/places.py:207
msgid "Trash"
msgstr "ຖັງຂີ້ເຍື້ອ"

#: ../lib/mate-menu-config.py:108
msgid "Custom Places:"
msgstr ""

#. Set 'heading' property for plugin
#: ../lib/mate-menu-config.py:110 ../mate_menu/plugins/system_management.py:55
msgid "System"
msgstr "ລະບົບ"

#: ../lib/mate-menu-config.py:113
msgid "Toggle Default Items:"
msgstr ""

#: ../lib/mate-menu-config.py:114 ../mate_menu/plugins/system_management.py:153
#: ../mate_menu/plugins/system_management.py:156
#: ../mate_menu/plugins/system_management.py:159
#: ../mate_menu/plugins/system_management.py:162
msgid "Package Manager"
msgstr ""

#: ../lib/mate-menu-config.py:115 ../mate_menu/plugins/system_management.py:172
msgid "Control Center"
msgstr "ສູນກາງ ການຄວບຄຸມ"

#: ../lib/mate-menu-config.py:116 ../mate_menu/plugins/system_management.py:179
msgid "Terminal"
msgstr ""

#: ../lib/mate-menu-config.py:117 ../mate_menu/plugins/system_management.py:193
msgid "Lock Screen"
msgstr "ລ້ອກຈໍ"

#: ../lib/mate-menu-config.py:118
msgid "Log Out"
msgstr "ອອກລະບົບ"

#: ../lib/mate-menu-config.py:119 ../mate_menu/plugins/system_management.py:211
msgid "Quit"
msgstr "ອອກ"

#: ../lib/mate-menu-config.py:121
msgid "Edit Place"
msgstr "ປັບສະຖານທີ່"

#: ../lib/mate-menu-config.py:122
msgid "New Place"
msgstr "ສະຖານທີ່ໃໝ່"

#: ../lib/mate-menu-config.py:123
msgid "Select a folder"
msgstr ""

#: ../lib/mate-menu-config.py:151
msgid "Keyboard shortcut:"
msgstr "ລັດເຂົ້າຫາແປ້ນພີມ:"

#: ../lib/mate-menu-config.py:157
msgid "Images"
msgstr "ຮູບພາບ"

#: ../lib/mate-menu-config.py:265
msgid "Name"
msgstr "ຊື່"

#: ../lib/mate-menu-config.py:266
msgid "Path"
msgstr "ເສ້ນທາງ"

#: ../lib/mate-menu-config.py:282
msgid "Desktop theme"
msgstr ""

#: ../lib/mate-menu-config.py:423 ../lib/mate-menu-config.py:454
msgid "Name:"
msgstr "ຊື່:"

#: ../lib/mate-menu-config.py:424 ../lib/mate-menu-config.py:455
msgid "Path:"
msgstr "ເສ້ນທາງ:"

#. i18n
#: ../mate_menu/plugins/applications.py:184
msgid "Search:"
msgstr ""

#: ../mate_menu/plugins/applications.py:188
msgid "All applications"
msgstr "ໂປຣເເກຣມທັ້ງຫງໝົດ"

#: ../mate_menu/plugins/applications.py:552
#, python-format
msgid "Search Google for %s"
msgstr "ກູໂກຊອກຫາສໍາລັບ %s"

#: ../mate_menu/plugins/applications.py:559
#, python-format
msgid "Search Wikipedia for %s"
msgstr "ວິກິປີດຽຍ ຊອກຫາສໍາລັບ %s"

#: ../mate_menu/plugins/applications.py:576
#, python-format
msgid "Lookup %s in Dictionary"
msgstr ""

#: ../mate_menu/plugins/applications.py:583
#, python-format
msgid "Search Computer for %s"
msgstr "ຊອກຫາຄອມເຕີ ສໍາລັບ %s"

#. i18n
#: ../mate_menu/plugins/applications.py:703
#: ../mate_menu/plugins/applications.py:768
msgid "Add to desktop"
msgstr ""

#: ../mate_menu/plugins/applications.py:704
#: ../mate_menu/plugins/applications.py:769
msgid "Add to panel"
msgstr ""

#: ../mate_menu/plugins/applications.py:706
#: ../mate_menu/plugins/applications.py:747
msgid "Insert space"
msgstr ""

#: ../mate_menu/plugins/applications.py:707
#: ../mate_menu/plugins/applications.py:748
msgid "Insert separator"
msgstr ""

#: ../mate_menu/plugins/applications.py:709
#: ../mate_menu/plugins/applications.py:772
msgid "Launch"
msgstr ""

#: ../mate_menu/plugins/applications.py:710
msgid "Remove from favorites"
msgstr ""

#: ../mate_menu/plugins/applications.py:712
#: ../mate_menu/plugins/applications.py:775
msgid "Edit properties"
msgstr ""

#. i18n
#: ../mate_menu/plugins/applications.py:746
msgid "Remove"
msgstr ""

#: ../mate_menu/plugins/applications.py:771
msgid "Show in my favorites"
msgstr ""

#: ../mate_menu/plugins/applications.py:773
msgid "Delete from menu"
msgstr ""

#: ../mate_menu/plugins/applications.py:817
msgid "Search Google"
msgstr ""

#: ../mate_menu/plugins/applications.py:824
msgid "Search Wikipedia"
msgstr ""

#: ../mate_menu/plugins/applications.py:834
msgid "Lookup Dictionary"
msgstr ""

#: ../mate_menu/plugins/applications.py:841
msgid "Search Computer"
msgstr ""

#: ../mate_menu/plugins/applications.py:1245
msgid ""
"Couldn't save favorites. Check if you have write access to ~/.config/mate-"
"menu"
msgstr ""

#: ../mate_menu/plugins/applications.py:1452
msgid "All"
msgstr "ທັງໝົດ"

#: ../mate_menu/plugins/applications.py:1452
msgid "Show all applications"
msgstr ""

#: ../mate_menu/plugins/system_management.py:167
msgid "Install, remove and upgrade software packages"
msgstr ""

#: ../mate_menu/plugins/system_management.py:176
msgid "Configure your system"
msgstr ""

#: ../mate_menu/plugins/system_management.py:190
msgid "Use the command line"
msgstr ""

#: ../mate_menu/plugins/system_management.py:201
msgid "Requires password to unlock"
msgstr ""

#: ../mate_menu/plugins/system_management.py:204
msgid "Logout"
msgstr ""

#: ../mate_menu/plugins/system_management.py:208
msgid "Log out or switch user"
msgstr ""

#: ../mate_menu/plugins/system_management.py:215
msgid "Shutdown, restart, suspend or hibernate"
msgstr ""

#: ../mate_menu/plugins/places.py:157
msgid ""
"Browse all local and remote disks and folders accessible from this computer"
msgstr ""

#: ../mate_menu/plugins/places.py:168
msgid "Open your personal folder"
msgstr ""

#: ../mate_menu/plugins/places.py:181
msgid "Browse bookmarked and local network locations"
msgstr ""

#: ../mate_menu/plugins/places.py:204
msgid "Browse items placed on the desktop"
msgstr ""

#: ../mate_menu/plugins/places.py:217
msgid "Browse deleted files"
msgstr ""

#: ../mate_menu/plugins/places.py:270
msgid "Empty trash"
msgstr ""

#. Set 'heading' property for plugin
#: ../mate_menu/plugins/recent.py:48
msgid "Recent documents"
msgstr ""

#: ../mate_menu/keybinding.py:170
msgid "Click to set a new accelerator key for opening and closing the menu.  "
msgstr ""

#: ../mate_menu/keybinding.py:171
msgid "Press Escape or click again to cancel the operation.  "
msgstr ""

#: ../mate_menu/keybinding.py:172
msgid "Press Backspace to clear the existing keybinding."
msgstr ""

#: ../mate_menu/keybinding.py:191
msgid "Pick an accelerator"
msgstr ""

#: ../mate_menu/keybinding.py:247
msgid "<not set>"
msgstr ""

#~ msgid "Software Manager"
#~ msgstr "ຜູ້ກໍາກັບຊອບເເວລ"

#~ msgid "Uninstall"
#~ msgstr "ຖອນຕິດຕັ້ງ"

#~ msgid "Please wait, this can take some time"
#~ msgstr "ກະລູນາລໍຖ້າ, ຈະໃຊ້ເວລາ"

#~ msgid "Advanced Gnome Menu"
#~ msgstr "Gnome ເມນູກ້າວໜ້າ"

#~ msgid "Application removed successfully"
#~ msgstr "ຖອນໂປຣເເກຣມສໍາເລັດ"

#~ msgid "Packages to be removed"
#~ msgstr "ແພກເກຈທີ່ຈະຖືກລຶບ"

#~ msgid "The following packages will be removed:"
#~ msgstr "ແພກເກຈຕໍ່ໄປນີ້ຈະຖືກລຶບ:"
